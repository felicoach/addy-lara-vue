export default [
  {
    title: 'Administrador',
    icon: 'HomeIcon',
    tag: '4',
    tagVariant: 'light-warning',
    children: [
      {
        title: 'Empresas',
        route: 'dashboard-ecommerce',
        //tag: '2',
        // tagVariant: 'light-warning',
        children: [
          {
            title: 'Listado',
            // route: 'dashboard-analytics',
          },
          {
            title: 'Crear',
            // route: 'dashboard-analytics',
          },
        ],
      },
      {
        title: 'Permisos FLM',
        // route: 'dashboard-flm',
        // tag: '2',
        // tagVariant: 'light-warning',
        children: [
          {
            title: 'Listado',
            route: 'dashboard-flm',
          },
          {
            title: 'Crear',
            // route: 'dashboard-analytics',
          },
        ],
      },
      {
        title: 'Modulos',
        route: 'dashboard-ecommerce',
        // tag: '2',
        // tagVariant: 'light-warning',
        children: [
          {
            title: 'Listado',
            route: 'dashboard-analytics',
          },
          {
            title: 'Crear',
            route: 'dashboard-analytics',
          },
        ],
      },
      {
        title: 'Roles',
        // route: 'dashboard-ecommerce',
        // tag: '2',
        // tagVariant: 'light-warning',
        children: [
          {
            title: 'Listado',
            // route: 'dashboard-analytics',
          },
          {
            title: 'Crear',
            // route: 'dashboard-analytics',
          },
        ],
      },
    ],
  },
]
